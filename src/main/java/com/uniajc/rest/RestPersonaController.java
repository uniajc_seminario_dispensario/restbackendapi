package com.uniajc.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uniajc.model.Persona;
import com.uniajc.repo.IPersonaRepo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/personas")
@Api(value="Sistema administrativo de Personas", description="Operaciones habilitadas para Personas en el sistema administrativo")
public class RestPersonaController {
	
	@Autowired
	private IPersonaRepo repo;
	
	@GetMapping
	@ApiOperation(value = "Listado de personas del sistema SAP", response = Persona.class)
	public List<Persona> listar(){
		return repo.findAll();
	}
	
	@GetMapping(value = "/{id}")
	@ApiOperation(value = "Listado de personas del sistema", response = Persona.class)
	public Optional<Persona> mostrar(@PathVariable("id") Integer id){
		return repo.findById(id);
	}
	
	@PostMapping
	@ApiOperation(value = "Crear una nueva Persona")
	public void insertar(@RequestBody Persona persona){
		repo.save(persona);
	}
	
	@PutMapping
	@ApiOperation(value = "Actualizar una persona")
	public void modificar(@RequestBody Persona persona){
		repo.save(persona);
	}
	
	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "Borrar una Persona")
	public void eliminar(@PathVariable("id") Integer id){
		repo.deleteById(id);
	}
}
