package com.uniajc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uniajc.model.Persona;
import com.uniajc.repo.IPersonaRepo;

@Controller
public class DemoController {

	@Autowired
	private IPersonaRepo repo;

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        
    	Persona p = new Persona();
    	p.setIdPersona(2);
    	p.setNombre(name);
    	
    	repo.save(p);
    	
    	model.addAttribute("name", name);
        return "greeting";
    }
    
    @GetMapping("/listado")
    public String greeting(Model model) {
    	
    	model.addAttribute("pesonas", repo.findAll());
        return "greeting";
    }
}
