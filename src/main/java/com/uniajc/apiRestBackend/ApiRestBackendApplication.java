package com.uniajc.apiRestBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.uniajc.config.SwaggerConfig;

@SpringBootApplication
@ComponentScan({"com.uniajc.controller", "com.uniajc.rest"})
@EntityScan("com.uniajc.model")
@EnableJpaRepositories("com.uniajc.repo")
@Import(SwaggerConfig.class)
public class ApiRestBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestBackendApplication.class, args);
	}

}
